const paginator = function () {
    var listElement;
    var perPage = 10;
    var curr = 0;

    function init() {
        listElement = document.getElementById('newStuff');
        var numItems = listElement.children.length;
        var numPages = Math.ceil(numItems / perPage);
        document.querySelector('.paginator-custom').setAttribute("data-curr", 0);

        let html = '';
        while (numPages > curr) {
            html += '<li><a href="javascript:void(0)" class="page_link">' + (curr + 1) + '</a></li>';
            curr++;
        }

        document.querySelector('.paginator-custom').innerHTML = html;
        document.querySelector('.paginator-custom .page_link:first-child').classList.add('active');

        for (element of listElement.children) {
            element.style.display = "none";
        }

        for (let i = 0; i < perPage; i++) {
            listElement.children[i].style.display = "block";
        }

        var pages = document.querySelectorAll('.paginator-custom li a');
        for (page of pages) {
            page.addEventListener("click", function (e) {
                var clickedPage = parseInt(this.text.valueOf()) - 1;
                goTo(clickedPage, perPage);
            })
        }
    }

    function goTo(page) {
        var startAt = page * perPage,
            endOn = startAt + perPage;

        for (element of listElement.children) {
            element.style.display = "none";
            unfade(element);
        }

        for (let i = startAt; i < endOn; i++) {
            (listElement.children[i] === undefined) ? 
            '' : listElement.children[i].style.display = "block";
        }

        document.querySelector('.paginator-custom').setAttribute("data-curr", page);
    }

    function unfade(element) {
        var op = 0.1; // initial opacity
        element.style.display = 'block';
        var timer = setInterval(function () {
            if (op >= 1) {
                clearInterval(timer);
            }
            element.style.opacity = op;
            element.style.filter = 'alpha(opacity=' + op * 100 + ")";
            op += op * 0.1;
        }, 10);
    }
    return {
        init
    }
}();
paginator.init();